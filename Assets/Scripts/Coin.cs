using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Coin : MonoBehaviour
{
    public static int coinsCount = 0;
    public GameObject ObjPuntos;
    public int puntosQueDa;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Moneda creada");
        Coin.coinsCount++;    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider collider) {

        if (collider.CompareTag("Player")) {

            ObjPuntos.GetComponent<Puntaje>().puntos += puntosQueDa;
            Destroy(gameObject);
//            contador = contador + 5;
//            Puntos = "SCORE: " + contador;
        }

    }

    void OnDestroy() {
        Debug.Log(Coin.coinsCount);

        Coin.coinsCount--;
        if (Coin.coinsCount <= 0) {
            SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
            Debug.Log("El juego a terminado");
        }
    }
}
